<div align="center">
  <a href="https://www.jcs-profile.com/" target="_blank">
    <img alt="Profile" src="https://www.jcs-profile.com/images/icons/profile.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="https://github.com/jcs090218" target="_blank">
    <img alt="GitHub" src="https://www.jcs-profile.com/images/icons/github.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="https://twitter.com/jenchieh94" target="_blank">
    <img alt="Twitter" src="https://www.jcs-profile.com/images/icons/twitter.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="https://www.linkedin.com/in/jen-chieh-shen-17a02780/" target="_blank">
    <img alt="LinkedIn" src="https://www.jcs-profile.com/images/icons/linkedin.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="mailto:jcs090218@gmail.com" target="_blank">
    <img alt="Mail" src="https://www.jcs-profile.com/images/icons/gmail.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="https://www.jcs-profile.com/blog/" target="_blank">
    <img alt="Blog" src="https://www.jcs-profile.com/images/icons/blog.svg" width="5%"/>
  </a>
</div>

<div align="center">
<a href="#">
  <img width="35%" src="https://readme-typing-svg.demolab.com?font=Ubuntu+Mono&pause=1000&color=F7F7F7&center=true&vCenter=true&width=435&lines=Information+above..." alt="Typing SVG" />
</a>
</div>
